package kotlin.silvioapps.com.estudokotlin

class Test constructor(val text:String){
    init {
        println("text "+text);
    }
}